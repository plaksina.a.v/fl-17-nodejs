const fs = require('fs');
const path = require('path');
const express = require('express');
const app = express();
const port = 8080;

const baseDir = './upload';

try {
  if (!fs.existsSync(baseDir)) {
    fs.mkdirSync(baseDir)
  }
} catch (err) {
  console.error(`Unable to create base directory due to: ${err}`);
  throw err;
}

app.use(express.json());

app.all('*', (req, res, next) => {
  const { headers, httpVersion, method, socket, url, body } = req;
  const { remoteAddress, remoteFamily } = socket;

  console.log(
    JSON.stringify({
      timestamp: Date.now(),
      httpVersion,
      method,
      url,
      remoteAddress,
      remoteFamily,
      headers,
      body
    })
  );
  next();
});

app.post('/api/files', (req, res) => {
    if (!req.body.filename) {
      res.status(400);
      res.send({message: 'Please specify \'filename\' parameter'});
      return;
    }

    if (!req.body.content) {
      res.status(400);
      res.send({message: 'Please specify \'content\' parameter'});
      return;
    }

    fs.writeFile(`${baseDir}/${req.body.filename}`, req.body.content, function (err) {
        if (err) {
          res.status(500);
          res.send({message: 'Server error'});
          return;
        }
        res.status(200);
        res.send({message: 'File created successfully'});
    });
});

app.get('/api/files', (req, res) => {
  fs.readdir(baseDir, (err, files) => {
    if (err) {
      res.status(500);
      res.send({message: 'Server error'});
      return;
    }
    res.status(200);
    res.send({
      message: 'Success',
      files
    });
  });
});

app.get('/api/files/:filename', (req, res) => {
  const filename = req.params.filename;
  const fileExt = path.extname(filename);
  const filePath = `${baseDir}/${filename}`;

  const stat = fs.statSync(filePath, { throwIfNoEntry: false });

  if (!stat) {
    res.status(400);
    res.send({ message: `No file with '${filename}' filename found` });
    return;
  }

  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      res.status(500);
      res.status({ message: 'Server error' });
      return;
    }
    res.status(200);
    res.send({
      message: 'Success',
      filename: filename,
      content: data,
      extension: fileExt,
      uploadedDate: stat.birthtime
    });
  });
});

app.delete('/api/files/:filename', (req, res) => {
  const filename = req.params.filename;
  const fileExt = path.extname(filename);
  const filePath = `${baseDir}/${filename}`;

  const stat = fs.statSync(filePath, { throwIfNoEntry: false });

  if (!stat) {
    res.status(400);
    res.send({ message: `No file with '${filename}' filename found` });
    return;
  }

  fs.unlink(filePath, (err) => {
    if (err) {
      res.status(500);
      res.status({ message: 'Server error' });
      return;
    }
    res.status(200);
    res.send({
      message: 'Success',
      filename: filename,
      extension: fileExt,
      uploadedDate: stat.birthtime
    });
  });
});

app.listen(port, () => {
  console.log(`NodeJS web-server is listening on port: ${port}`)
});
